package com.ait.stockSolutions;

public interface StockBroker {
	void buy(Stock stock, int quantity);
	void sell(Stock stock, int quantity);
	double getQoute(Stock stock);
}
