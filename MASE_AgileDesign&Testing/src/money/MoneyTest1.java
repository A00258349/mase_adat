package money;

import static org.junit.Assert.*;
import org.junit.Test;
import static junitparams.JUnitParamsRunner.$;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class MoneyTest1 {
	
	
	@Parameters
	private static final Object[] getEqual(){
		return $(
				$(
						$(10,"USD"),
						$(11,"USD")
				),   
				$(
						$(12,"USD"),
						$(13,"USD")
				)
		);
	}
	
	@Test
	@Parameters(method="getEqual")
	public void testEqualValues(Object[] a, Object[] b) {
		System.out.println(a[0]+","+a[1]);
		System.out.println(b[0]+","+b[1]);
		
	}
	
	
}
