package calculator;

import static junitparams.JUnitParamsRunner.$;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class CalculatorTestCopy {

	@Parameters
	private static final Object[] getValidInvestmentAmountsAndPeriods() {
		return $($(1000, 3, 1061.21), $(1001, 4, 1083.51), $(2000, 5, 2208.16), $(2998, 6, 3376.23),
				$(2999, 6, 3377.36), $(9999, 9, 18382.75), $(10000, 10, 19671.51));
	}

	@Parameters
	private static final Object[] getInvalidInvestmentAmounts() {
		return $($(999, 3), $(10001, 3));
	}

	@Parameters
	private static final Object[] getInvalidInvestmentPeriods() {
		return $($(1000, 2), $(10000, 11));
	}

	@Test
	@Parameters(method = "getValidInvestmentAmountsAndPeriods")
	public void testValidInvestmentAmounts(int investment, int term, double finalAmount) {
		assertEquals(finalAmount, Calculator.calculateInvestmentValue(term, investment), 0.01);
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "getInvalidInvestmentAmounts")
	public void testInvalidInvestmentAmountsExpectingException(int amount, int term) {
		Calculator.calculateInvestmentValue(term, amount);
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "getInvalidInvestmentPeriods")
	public void testInvalidInvestmentPeriodsExpectingException(int amount, int term) {
		Calculator.calculateInvestmentValue(term, amount);
	}
}