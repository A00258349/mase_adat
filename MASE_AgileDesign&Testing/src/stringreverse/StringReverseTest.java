package stringreverse;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import junit.runner.*;

class StringReverseTest {
	@Test
	void validString() {
		assertEquals("nayR", StringReverse.reverse("Ryan"));
	}
	@Test
	void nullString() {
		assertEquals("nayR", StringReverse.reverse(null));
	}
	@Test
	void blankString() {
		assertEquals("", StringReverse.reverse(""));
	}
	
	@Test
	void spacesString() {
		assertEquals("   ", StringReverse.reverse("   "));
	}
	
	@Test
	void numbersString() {
		assertEquals("321", StringReverse.reverse("123"));
	}
	
	@Test
	void specialCharsString() {
		assertEquals("=!@", StringReverse.reverse("@!="));
	}
	
	@Test
	void palindromeString() {
		assertEquals("tacocat", StringReverse.reverse("tacocat"));
	}
	
}
