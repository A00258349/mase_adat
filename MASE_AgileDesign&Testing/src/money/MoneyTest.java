/**
 * 
 */
package money;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MoneyTest {

	
	@Test
	void constructorShouldSetAmountAndCurrency() {
		Money usd = new Money(10, "USD");
		Money eur = new Money(20, "EUR");
		Money gbp = new Money(15, "GBP");
		
		assertEquals(10, usd.getAmount());
		assertEquals(20, eur.getAmount());
		assertEquals(15, gbp.getAmount());
	}

	@Test
	void amountAndCurrencyEqual() {
		Money usd1 = new Money(10, "USD");
		Money usd2 = new Money(10, "USD");
		Money eur1 = new Money(20, "EUR");
		Money eur2 = new Money(20, "EUR");
		Money gbp1 = new Money(15, "GBP");
		Money gbp2 = new Money(15, "GBP");
		
		assertTrue(usd1.equals(usd2));
		assertTrue(eur1.equals(eur2));
		assertTrue(gbp1.equals(gbp2));
	}

	@Test
	void currencyEqualAndAmountsNotEqual() {
		Money usd1 = new Money(15, "USD");
		Money usd2 = new Money(10, "USD");
		Money eur1 = new Money(20, "EUR");
		Money eur2 = new Money(10, "EUR");
		Money gbp1 = new Money(15, "GBP");
		Money gbp2 = new Money(20, "GBP");
		
		assertFalse(usd1.equals(usd2));
		assertFalse(eur1.equals(eur2));
		assertFalse(gbp1.equals(gbp2));
	}
	
	@Test
	void amountsEqualAndCurrencyNotEqual() {
		Money usd1 = new Money(15, "USD");
		Money usd2 = new Money(10, "USD");
		Money eur1 = new Money(10, "EUR");
		Money eur2 = new Money(20, "EUR");
		Money gbp1 = new Money(20, "GBP");
		Money gbp2 = new Money(15, "GBP");
		
		assertFalse(usd2.equals(eur1));
		assertFalse(eur1.equals(gbp2));
		assertFalse(usd1.equals(gbp1));
	}
	
	
}
