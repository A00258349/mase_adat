package samplejunitassess2;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class MarkCheckerTest {

	MarkChecker m = new MarkChecker();
	

	

	@Parameters
	private static final Object[] invalidMarksParams() {
		Object[] objects = new Object[2];
		objects[0] = new Object[] { new int[] { -1, 10, 10, 10, 10 },new int[] { 10, 10, 10, 10, 10 }};	
		objects[1] = new Object[] { new int[] { 10, 10, 21, 10, 10 },new int[] { 10, 10, 10, 10, 10 }};
		return objects;
		
	}
	
	@Parameters
	private static final Object[] validMarksParams() {
		Object[] objects = new Object[2];
		objects[0] = new Object[] { new int[] { -1, 10, 10, 10, 10 },new int[] { 10, 10, 10, 10, 10 }};	
		objects[1] = new Object[] { new int[] { 10, 10, 21, 10, 10 },new int[] { 10, 10, 10, 10, 10 }};
		return objects;
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "invalidMarksParams")
	public void testInvalidScores(int[] marker1, int[] marker2) {
		
		MarkChecker m = new MarkChecker();
		m.checkMarks(marker1, marker2);
	}


}
