package money;

import static junitparams.JUnitParamsRunner.$;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import junitparams.*;

class MoneyParamsTest {
	@Parameters
	private static final Object[] getMoney() {
		return $(
				$(10,"USD"),       
				$(20,"EUR"),   
				$(15,"GBP")
		);
}
	@ParameterizedTest
    @MethodSource("getMoney")
	public void constructorShouldSetAmountAndCurrency(int amount, String currency) {
		Money money = new Money(amount, currency);
		assertEquals(amount,  money.getAmount());
		assertEquals(currency,  money.getCurrency());
	}

}
