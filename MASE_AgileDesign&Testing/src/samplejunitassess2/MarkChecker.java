package samplejunitassess2;

public class MarkChecker {
	public final int DIFFERENCE_L1 = 3;
	public final int DIFFERENCE_L2 = 10;
	public int checkMarks(int[] markerOne, int[] markerTwo) throws IllegalArgumentException {
		int difference = 0;
		for(int i = 0; i < markerOne.length; i++) {
			if(Math.abs(markerOne[i] - markerTwo[i]) > difference){
				difference = Math.abs(markerOne[i] - markerTwo[i]);	
			}
			if(markerOne[i] > 20 || markerTwo[i] > 20 || markerOne[i] < 0 || markerTwo[i] < 0 ) {
				throw new IllegalArgumentException("Marks entered cannot be above 20!");
			}
		}
		
		if(difference <= DIFFERENCE_L1) {
			return 0;
		} else if(difference <= DIFFERENCE_L2) {
			return 1;
		} else {
			return 0;
		}

	}

}
