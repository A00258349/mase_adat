package calculator;

import java.text.DecimalFormat;

/**
 * @author A00258349
 *
 */
public class Calculator {
	
	public static final double INTEREST_BAND_LOW = 1.02;
	public static final double INTEREST_BAND_MID = 1.05;
	public static final double INTEREST_BAND_HIGH = 1.07;

	public static Double calculateInvestmentValue(int term, double startAmount) throws IllegalArgumentException {

		double rate = 0.0;
		if(term < 3 || term > 10) {
			throw new IllegalArgumentException("Term must be between 3 and 10 years!");
		}
		else if(startAmount < 1000 || startAmount > 10000) {
			throw new IllegalArgumentException("Start amount must be between 1000 and 10000");
		}
		else {
			if (startAmount < 3000) {
				rate = INTEREST_BAND_LOW;
			} else if (startAmount < 5000) {
				rate = INTEREST_BAND_MID;
			} else {
				rate = INTEREST_BAND_HIGH;
			}
			for (int i = 0; i < term; i++) {
				startAmount *= rate;
			}

			DecimalFormat twoDForm = new DecimalFormat("#.##");

			return Double.valueOf(twoDForm.format(startAmount));
		}
		

		

	}
}
