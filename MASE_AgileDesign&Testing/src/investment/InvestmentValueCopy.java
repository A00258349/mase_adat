/**
 * 
 */
package investment;

import java.text.DecimalFormat;

/**
 * @author A00258349
 *
 */
public class InvestmentValueCopy {
	
	public static final double INTEREST_BAND_LOW = 1.02;
	public static final double INTEREST_BAND_MID = 1.05;
	public static final double INTEREST_BAND_HIGH = 1.07;

	public static double calculateInvestmentValue(int term, double startAmount) {

		double rate = 0.0;
		if (startAmount < 3000) {
			rate = INTEREST_BAND_LOW;
		} else if (startAmount < 5000) {
			rate = INTEREST_BAND_MID;
		} else {
			rate = INTEREST_BAND_HIGH;
		}

		for (int i = 0; i < term; i++) {
			startAmount *= rate;
		}

		DecimalFormat twoDForm = new DecimalFormat("#.##");

		return Double.valueOf(twoDForm.format(startAmount));

	}
}
