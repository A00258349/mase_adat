/**
 * Author: A00258349
 */
package com.ait.delivery;

public class DeliveryCharge {
	private static final double LOW_RATE = 8.99;
	private static final double MID_RATE = 3.99;
	private static final double HIGH_RATE = 0.00;

	public double calculateCharge(double orderAmount) throws IllegalArgumentException {
		
		if (orderAmount > 10_000 || orderAmount <= 0) {
			throw new IllegalArgumentException(orderAmount + "is not a valid amount");
		}

		if (orderAmount < 100.0) {
			return LOW_RATE;
		} else if (orderAmount < 200.0) {
			return MID_RATE;
		} else {
			return HIGH_RATE;
		}

	}
}
