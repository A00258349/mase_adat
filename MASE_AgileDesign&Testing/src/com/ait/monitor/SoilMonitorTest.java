/**
 * Author: A00258349
 */
package com.ait.monitor;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class SoilMonitorTest {

	private SoilMonitor sm;

	@Before
	public void setUp() {
		sm = new SoilMonitor();
	}

	@Parameters
	private Object[] invalidNumOfReadingsParams() {
		Object[] objects = new Object[2];
		objects[0] = new Object[] { new int[] { 13, 14, 14 } };
		objects[1] = new Object[] { new int[] { 13, 14, 14, 12, 15 } };
		return objects;
	}

	@Parameters
	private Object[] validReadingsParams() {
		Object[] objects = new Object[10];
		objects[0] = new Object[] { new int[] { 0, 0, 0, 0 }, 2 };
		objects[1] = new Object[] { new int[] { 1, 1, 1, 1 }, 2 };
		objects[2] = new Object[] { new int[] { 50, 50, 50, 50 }, 2 };
		objects[3] = new Object[] { new int[] { 51, 51, 51, 51 }, 1 };
		objects[4] = new Object[] { new int[] { 52, 52, 51, 52 }, 1 };
		objects[5] = new Object[] { new int[] { 100, 100, 100, 100 }, 1 };
		objects[6] = new Object[] { new int[] { 101, 101, 101, 101 }, 0 };
		objects[7] = new Object[] { new int[] { 102, 102, 102, 102 }, 0 };
		objects[8] = new Object[] { new int[] { 149, 149, 149, 149 }, 0 };
		objects[9] = new Object[] { new int[] { 150, 150, 150, 150 }, 0 };
		return objects;
	}

	@Parameters
	private Object[] invalidReadingsParams() {
		Object[] objects = new Object[2];
		objects[0] = new Object[] { new int[] { -1, 0, 0, 0 } };
		objects[1] = new Object[] { new int[] { 151, 0, 0, 0 } };

		return objects;
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "invalidNumOfReadingsParams")
	public void testInvalidNumberOfReadings(int[] moistureContent) {
		sm.moistureLevelCheck(moistureContent);
	}

	@Test
	@Parameters(method = "validReadingsParams")
	public void testValidReadings(int[] moistureContent, int expected) {
		assertEquals(expected, sm.moistureLevelCheck(moistureContent));
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "invalidReadingsParams")
	public void testInvalidReadings(int[] moistureContent) {
		sm.moistureLevelCheck(moistureContent);
	}

}
