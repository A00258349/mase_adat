package com.ait.retailers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.mockito.verification.VerificationMode;

public class BiggestBazarRetailTest {
	
	private Inventory inventory;
	private Item item;
	private Offer offer;
	private PublicAddressSystem pas;
	ArrayList<Item> expiryItems;
	BiggestBazarRetail bbr;
	
	@Before
	public void setup() {
		inventory = mock(Inventory.class);
		offer = mock(Offer.class);
		pas = mock(PublicAddressSystem.class);
		bbr = new BiggestBazarRetail(inventory, pas);
	}
	
	@Test
	public void testIfInventoryIsAnInventory() {
		assertTrue(inventory instanceof Inventory);
	}

	@Test
	public void testIfOfferIsAnOffer() {
		assertTrue(offer instanceof Offer);
	}
	@Test
	public void testIssueDiscountForOneItem() {
		expiryItems = new ArrayList<Item>();
		item = new Item("000", "Apple iPhone X", 1000.0, 700.0);
		expiryItems.add(item);
		
		when(inventory.getItemsExpireInAMonth()).thenReturn(expiryItems);
		when(inventory.itemsUpdated()).thenReturn(1);
		
		bbr.issueDiscountForItemsExpireIn30Days(0.1);
		
		verify(inventory).getItemsExpireInAMonth();
		verify(inventory).update(item, 900.0);
		verify(pas).announce(isA(Offer.class));
	}
	
	@Test
	public void testNoDiscountIssuedForOneItem() {
		expiryItems = new ArrayList<Item>();
		item = new Item("001", "Apple", 700.0, 900.0);
		expiryItems.add(item);
		
		when(inventory.getItemsExpireInAMonth()).thenReturn(expiryItems);
		when(inventory.itemsUpdated()).thenReturn(1);
		
		assertEquals(1,bbr.issueDiscountForItemsExpireIn30Days(0.1));
		
		verify(inventory).itemsUpdated();
		verify(inventory, never()).update(isA(Item.class), isA(Double.class));
		verify(pas, never()).announce(isA(Offer.class));
	}
	
	@Test
	public void testIssueDiscountForOneItems() {
		expiryItems = new ArrayList<Item>();
		item = new Item("001", "Samsung", 700.0, 900.0);
		Item item2 = new Item("002", "Apple", 1000.0, 700.0);
		Item item3 = new Item("003", "OnePlus", 900.0, 600.0);
		expiryItems.add(item);
		expiryItems.add(item2);
		expiryItems.add(item3);
		
		when(inventory.getItemsExpireInAMonth()).thenReturn(expiryItems);
		when(inventory.itemsUpdated()).thenReturn(3);
		
		bbr.issueDiscountForItemsExpireIn30Days(0.1);
		
		verify(inventory).itemsUpdated();
		verify(inventory, times(2)).update(isA(Item.class), isA(Double.class));
		verify(pas, times(2)).announce(isA(Offer.class));
	}
	
	
	


}
