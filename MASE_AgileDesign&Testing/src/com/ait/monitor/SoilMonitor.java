/**
 * Author: A00258349
 */
package com.ait.monitor;

public class SoilMonitor {
	public int moistureLevelCheck(int[] moistureContent) throws IllegalArgumentException {

		if (moistureContent.length != 4) {
			throw new IllegalArgumentException("System faulty");
		}
		int sum = 0;

		for (int i = 0; i < 4; i++) {
			if (moistureContent[i] < 0 || moistureContent[i] > 150) {
				throw new IllegalArgumentException("System faulty");
			}

			sum += moistureContent[i];
		}

		int avg = (int) (sum / 4);

		if (avg <= 50) {
			return 2;
		} else if (avg <= 100) {
			return 1;
		} else {
			return 0;
		}
	}
}
