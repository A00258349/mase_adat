package calculator;

import static junitparams.JUnitParamsRunner.$;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class CalculatorTest {
	@Parameters
	private static final Object[] invalidTermParams() {
		return $($(-1, 1000), $(2, 1000), $(11, 1000));
	}

	@Parameters
	private static final Object[] invalidStartAmountParams() {
		return $($(3, -1.0), $(3, 0.0), $(3, 999.0), $(3, 10001.0));
	}

	@Parameters
	private static final Object[] validTermParams() {
		return $($(3, 1000, 1061.21), $(10, 1000, 1218.99));
	}

	@Parameters
	private static final Object[] validStartAmountParams() {
		return $($(3, 1000.0, 1218.99), $(3, 1001.0, 1220.21), $(3, 9999.0, 19669.55), $(3, 10000.0, 19671.51));
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "invalidStartAmountParams")
	public void testInvalidStartAmount(int term, double startAmount) {
		Calculator.calculateInvestmentValue(term, startAmount);
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "invalidTermParams")
	public void testInvalidTerm(int term, double startAmount) {
		Calculator.calculateInvestmentValue(term, startAmount);
	}

	@Test
	@Parameters(method = "validTermParams")
	public void testValidTerm(int term, double startAmount, double expected) {
		assertEquals(expected, Calculator.calculateInvestmentValue(term, startAmount), 0.1);
	}

	@Test
	@Parameters(method = "validTermParams")
	public void testValidStartAmount(int term, double startAmount, double expected) {
		assertEquals(expected, Calculator.calculateInvestmentValue(term, startAmount), 0.1);
	}

}
