/**
 * 
 */
package investment;

import static org.junit.jupiter.api.Assertions.*;

import java.text.DecimalFormat;

import org.junit.jupiter.api.Test;

/**
 * @author A00258349
 *
 */
class InvestmentValueTest {
	DecimalFormat twoDForm = new DecimalFormat("#.##");
	
	@Test
	void Amount1000Term3() {
		assertEquals(1061.21, InvestmentValue.calculateInvestmentValue(3, 1000), 0.01);
	}
	
	@Test
	void Amount2999Term3() {
		assertEquals(3182.56, InvestmentValue.calculateInvestmentValue(3, 2999), 0.01);
	}
	
	@Test
	void Amount2000Term3() {
		assertEquals(2122.42, InvestmentValue.calculateInvestmentValue(3, 2000), 0.01);
	}
	
	@Test
	void Amount3000Term3() {
		assertEquals(3472.88, InvestmentValue.calculateInvestmentValue(3, 3000), 0.01);
	}
	
	@Test
	void Amount4999Term3() {
		assertEquals(5786.97, InvestmentValue.calculateInvestmentValue(3, 4999), 0.01);
	}
	@Test
	void Amount4000Term3() {
		assertEquals(4630.50, InvestmentValue.calculateInvestmentValue(3, 4000), 0.01);
	}
	
	@Test
	void Amount5000Term3() {
		assertEquals(6125.22, InvestmentValue.calculateInvestmentValue(3, 5000), 0.01);
	}
	
	@Test
	void Amount10000Term3() {
		assertEquals(12250.43, InvestmentValue.calculateInvestmentValue(3, 10000), 0.01);
	}
	
	@Test
	void Amount7500Term3() {
		assertEquals(9187.82, InvestmentValue.calculateInvestmentValue(3, 7500), 0.01);
	}
	
	@Test
	void Amount2000Term10() {
		assertEquals(2437.99, InvestmentValue.calculateInvestmentValue(10, 2000), 0.01);
	}
	
	@Test
	void Amount2000Term7() {
		assertEquals(2297.37, InvestmentValue.calculateInvestmentValue(7, 2000), 0.01);
	}


}
