/**
 * Author: A00258349
 */
package com.ait.delivery;

import static junitparams.JUnitParamsRunner.$;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class DeliveryChargeTest {

	DeliveryCharge dc;

	@Before
	public void setUp() {
		dc = new DeliveryCharge();
	}

	@Parameters
	private Object[] validOrderAmountParams() {
		return $($(0.01, 8.99), $(99.99, 8.99), $(100, 3.99), $(101, 3.99), $(199.99, 3.99), $(200, 0.00), $(201, 0.00),
				$(9_999, 0.00), $(10_000, 0.00));
	}

	@Parameters
	private Object[] invalidOrderAmountParams() {
		return $($(0), $(10001));
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "invalidOrderAmountParams")
	public void testInvalidOrderAmount(double orderAmount) {
		dc.calculateCharge(orderAmount);
	}

	@Test
	@Parameters(method = "validOrderAmountParams")
	public void testValidOrderAmount(double orderAmount, double expected) {
		assertEquals(expected, dc.calculateCharge(orderAmount), 0.01);
	}
}
