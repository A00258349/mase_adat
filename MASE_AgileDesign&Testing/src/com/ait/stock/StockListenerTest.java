package com.ait.stock;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;


public class StockListenerTest {
	//TO DO
	private StockBroker broker;
	private Stock stock;
	private StockListener listener;
	@Before
	public void setup() {
		 broker = mock(StockBroker.class);
		 stock = new Stock("1", 1000.0);
		 listener = new StockListener(broker);
	}
	
	@Test
	public void testIfStockBrokerIsAStockBroker() {
		assertTrue(broker instanceof StockBroker);
	}
	
	@Test
	public void sellStocksWhenPriceGoesUp(){
		listener = new StockListener(broker);
		when(broker.getQoute(stock)).thenReturn(1001.0);
		listener.takeAction(stock);
		verify(broker).sell(stock, 10);
	}
	@Test
	public void buyStocksWhenPriceGoesDown() {
		when(broker.getQoute(stock)).thenReturn(999.0);
		listener.takeAction(stock);
		verify(broker).buy(stock, 100);
	}
	
}
